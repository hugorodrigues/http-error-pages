# Generate generic HTML error pages

# How to
From you POSIX shell, call the generate.sh script. It will ask you your team name and email contact.

When finished, just deploy the files under output/ into your server.

# Deploy
## Nginx
```shell
  error_page 400 /400.html;
  error_page 401 /401.html;
  error_page 403 /403.html;
  error_page 404 /404.html;
  error_page 405 /405.html;
  error_page 406 /406.html;
  error_page 408 /407.html;
  error_page 409 /409.html;
  error_page 410 /410.html;
  error_page 413 /413.html;
  error_page 415 /415.html;
  error_page 417 /417.html;
  error_page 426 /426.html;
  error_page 500 /500.html;
  error_page 501 /501.html;
  error_page 502 /502.html;
  error_page 503 /503.html;
  error_page 504 /504.html;
  error_page 505 /505.html;
  location /400.html { root /usr/share/nginx/html; internal; }
  location /401.html { root /usr/share/nginx/html; internal; }
  location /403.html { root /usr/share/nginx/html; internal; }
  location /404.html { root /usr/share/nginx/html; internal; }
  location /405.html { root /usr/share/nginx/html; internal; }
  location /406.html { root /usr/share/nginx/html; internal; }
  location /408.html { root /usr/share/nginx/html; internal; }
  location /409.html { root /usr/share/nginx/html; internal; }
  location /410.html { root /usr/share/nginx/html; internal; }
  location /413.html { root /usr/share/nginx/html; internal; }
  location /415.html { root /usr/share/nginx/html; internal; }
  location /417.html { root /usr/share/nginx/html; internal; }
  location /426.html { root /usr/share/nginx/html; internal; }
  location /500.html { root /usr/share/nginx/html; internal; }
  location /501.html { root /usr/share/nginx/html; internal; }
  location /502.html { root /usr/share/nginx/html; internal; }
  location /503.html { root /usr/share/nginx/html; internal; }
  location /504.html { root /usr/share/nginx/html; internal; }
  location /505.html { root /usr/share/nginx/html; internal; }
```

# Kudos
* [generic-http-error-pages](https://github.com/ronaldpim/generic-http-error-pages) from [ronaldpim](https://github.com/ronaldpim) - Ideia source
* [This gist](https://gist.github.com/pitch-gist/2999707) from [pitch-gist](https://github.com/pitch-gist) - HTML template

# Contribute
You can add more code or evolve the generate script, but to commit to this repo please sign the WAIVER file

```
echo -e "## your email\n" >> AUTHORS
gpg --detach-sign --sign --armor --no-version -o - WAIVER >> AUTHORS 
```

And send me the public GPG key for validation.
