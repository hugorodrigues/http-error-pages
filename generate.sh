#!/bin/sh

read -p "Team name: " tname
read -p "Team email: " tmail


errors=(
    "400=Bad Request"
    "401=Unauthorized"
    "403=Forbidden"
    "404=Not Found"
    "405=Method Not Allowed"
    "406=Not Acceptable"
    "408=Request Timeout"
    "409=Conflict"
    "410=Gone"
    "413=Payload Too Large"
    "414=URI Too Long"
    "415=Unsupported Media Type"
    "417=Expectation Failed"
    "426=Upgrade Required"
    "500=Internal Server Error"
    "501=Not Implemented"
    "502=Bad Gateway"
    "503=Service Unavailable"
    "504=Gateway Timeout"
    "505=HTTP Version Not Supported"
)

for error in "${errors[@]}"; do
    ecode=$(echo "${error}" |cut -d"=" -f1)
    emsg=$(echo "${error}" |cut -d"=" -f2)
    outf="output/${ecode}.html"
    cp generic.html "${outf}"
    sed -i "s/__ERRORCODE__/${ecode}/g" "${outf}"
    sed -i "s/__ERRORMESSAGE__/${emsg}/g" "${outf}"
    sed -i "s/__TEAMNAME__/${tname}/g" "${outf}"
    sed -i "s/__TEAMMAIL__/${tmail}/g" "${outf}"
    echo "generated ${outf}"
done
